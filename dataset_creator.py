import json
import pprint
import random
import string
import argparse
import requests


class DataGen(object):
    """Read a rawdataset as source, randomise contents and create a set to insert into mongo"""
    PEOPLE_URL = "https://randomuser.me/api/"

    def __init__(self, params):
        self.source_path = params.source
        self.blocks = params.datablock
        self.output = params.output
        self.pp = pprint.PrettyPrinter(indent=4)

    def load(self, path):
        """Load source json file"""
        with open(path + '/rawd.json') as source:
            self.source = json.load(source)

    def randomise(self, count):
        """ Load people information from randomuser API and insert into JSON documents"""
        samples = []
        blocks = []
        for index in range(0, count):
            req = requests.get(self.PEOPLE_URL)
            pid = '%030x' % random.randrange(16**30)
            person = req.json()['results'][0]
            folder = "/source/beamline/data{0}/{1}/disk{0}/folder{2}".format(
                index, self.random_string(), index + 1)
            base = self.source.copy()
            base['_id'] = pid
            base['scientificMetadata']['scanParameters'][
                'File Prefix'] = "BEAM_{}".format(self.random_string(2))
            base['owner'] = "{}. {} {}".format(person['name']['title'],
                                               person['name']['first'],
                                               person['name']['last'])
            base['principalInvestigator'] = base['contactEmail'] = base[
                'ownerEmail'] = person['email']
            base['sourceFolder'] = folder
            base['creationLocation'] = folder
            base['scientificMetadata']['scanParameters'][
                'Sample folder'] = folder
            #self.pp.pprint(base)
            samples.append(base)
            if (self.blocks > 0):
                blocks += self.generate_blocks(pid, self.blocks)
        with open(self.output + '/raw_datasets.bson', 'w') as f:
            json.dump(samples, f)
        with open(self.output + '/datablocks.bson', 'w') as f:
            json.dump(blocks, f)

    def random_string(self, length=5):
        x = ''.join(random.choice(string.ascii_letters) for _ in range(length))
        return x

    def generate_blocks(self, pid, count):
        blocks = []
        with open(self.source_path + '/datablock.json') as db:
            block = json.load(db)
            for index in range(0, random.randrange(0, count)):
                b = block.copy()
                datafile_list = []
                file_count = random.randrange(1, 200)
                size = 0
                for count in range(0, file_count):
                    d_file = {
                        'path':
                        "/{}/{}/{}".format(self.random_string(), self.random_string(), self.random_string()),
                        'size':
                        random.randrange(100, 250000)
                    }
                    size += d_file['size']
                    datafile_list.append(d_file)
                b['dataFileList'] = datafile_list
                b['size'] = size
                b['rawDatasetId'] = pid
                b['datasetId'] = pid
                b['archivePath'] ="/{0}/{0}/{0}".format(self.random_string())
                blocks.append(b)
        return blocks


if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('--count', type=int, default=10)
    p.add_argument('--datablock', type=int, default=0)
    p.add_argument('--source', default='./input/')
    p.add_argument('--output', default='./output/')
    args = p.parse_args()
    DAT = DataGen(args)
    DAT.load(args.source)
    DAT.randomise(args.count)
